function guardarDatosUsuario(){
  var nombre = document.getElementById( "txtNombre" ).value;
  var email = document.getElementById( "txtEmail" ).value;
  var dni = document.getElementById( "txtDni" ).value;
  localStorage.setItem( "nombre", nombre );
  localStorage.setItem( "email", email );
  localStorage.setItem( "dni", dni );
  sessionStorage.setItem( "nombre", nombre );
  sessionStorage.setItem( "email", email );
  sessionStorage.setItem( "dni", dni );
  var usuario = { "nombre": nombre, "email": email, "dni": dni };
  localStorage.setItem( "usuarioST", JSON.stringify( usuario ) );
  sessionStorage.setItem( "usuarioST", JSON.stringify( usuario ) );
}

function recuperarDatosUsuario(){
  var nombreL = localStorage.getItem( "nombre" );
  var emailL = localStorage.getItem( "email" );
  var dniL = localStorage.getItem( "dni" );
  var usuarioL = JSON.parse( localStorage.getItem( "usuarioST" ) );
  var nombreS = sessionStorage.getItem( "nombre" );
  var emailS = sessionStorage.getItem( "email" );
  var dniS = sessionStorage.getItem( "dni" );
  var usuarioS = JSON.parse( sessionStorage.getItem( "usuarioST" ) );
  var s = "Session --> Nombre: " + nombreS + " Email: " + emailS + " DNI: " + dniS;
  var l = "Local --> Nombre: " + nombreL + " Email: " + emailL + " DNI: " + dniL;
  alert( l + "\n\n" + s );
  alert( usuarioL.email );
}
